package de.foodsharing.services

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.UserAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.Conversation
import de.foodsharing.model.ConversationDetailResponse
import de.foodsharing.model.ConversationResponse
import de.foodsharing.model.MessageResponse
import de.foodsharing.model.User
import de.foodsharing.notifications.NotificationFactory
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomUser
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.Date

class ConversationServiceTest {

    @Mock
    lateinit var conversationsAPI: ConversationsAPI

    @Mock
    lateinit var userAPI: UserAPI

    @Mock
    lateinit var ws: WebsocketAPI

    @Mock
    lateinit var auth: AuthService

    @Mock
    lateinit var notificationFactory: NotificationFactory

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch profile after incoming web socket message`() {
        // Variable which is set to true if any error occurred
        var hasFailed = false

        val currentUser = createRandomUser()

        val oldMessage = MessageResponse(1, "old", Date(), currentUser.id)

        val newUser = createRandomUser()
        val newMessage = MessageResponse(2, "new", Date(), newUser.id)

        val conversationResponse = ConversationResponse(
            1, null, false, listOf(currentUser.id), oldMessage, listOf(oldMessage)
        )

        val conversationDetailResponse = ConversationDetailResponse(
            conversation = conversationResponse,
            profiles = listOf(currentUser)
        )

        val conversation = conversationDetailResponse.conversation
            .toConversation(listOf(currentUser).associateBy { it.id })

        whenever(auth.currentUser()) doAnswer { Observable.create { it.onNext(currentUser) } }

        val profileSubject = PublishSubject.create<User>()
        whenever(userAPI.getUser(newUser.id)) doAnswer { profileSubject }

        val conversationSubject = PublishSubject.create<ConversationDetailResponse>()
        whenever(conversationsAPI.get(any(), any())) doAnswer { conversationSubject }

        val wsSubject = PublishSubject.create<WebsocketAPI.Message>()
        whenever(ws.subscribe()) doAnswer { wsSubject }

        whenever(notificationFactory.removeConversationNotification(any())) doAnswer {}

        val service = ConversationsService(
            conversationsAPI,
            userAPI,
            ws,
            auth,
            notificationFactory
        )

        val loadNextEvents = PublishSubject.create<Any>()
        val errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>) = {
            it.doOnNext {
                hasFailed = true
            }.flatMap {
                Observable.empty<Any>()
            }
        }

        var lastConversation: Conversation? = null
        service.conversationPaged(conversation.id, loadNextEvents, errorHandler).subscribe({
            lastConversation = it
        }, {
            hasFailed = true
        })

        Assert.assertNull(lastConversation)

        conversationSubject.onNext(conversationDetailResponse)
        Assert.assertEquals(conversation, lastConversation)
        lastConversation = null

        wsSubject.onNext(WebsocketAPI.ConversationMessage(conversation.id, newMessage))
        Assert.assertNull(lastConversation)

        profileSubject.onNext(newUser)
        // Check that the author of the message is correclty resolved
        Assert.assertEquals(newUser, lastConversation?.messages?.first()?.author)

        Assert.assertFalse(hasFailed)
    }
}
