package de.foodsharing.ui.picture

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_picture.picture_activity_view
import kotlinx.android.synthetic.main.activity_picture.picture_toolbar

class PictureActivity : BaseActivity() {

    companion object {
        const val EXTRA_PICTURE_FILE = "pictureFile"
        const val EXTRA_PICTURE_URL = "pictureUrl"
        const val EXTRA_SHOW_DELETE_BUTTON = "show_delete_button"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture)
        setSupportActionBar(picture_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        val path = if (intent.hasExtra(EXTRA_PICTURE_FILE)) {
            val filePath = intent.getStringExtra(EXTRA_PICTURE_FILE)
            "file:$filePath"
        } else if (intent.hasExtra(EXTRA_PICTURE_URL)) {
            intent.getStringExtra(EXTRA_PICTURE_URL)
        } else null

        path?.let {
            Glide.with(this)
                .load(path)
                .into(picture_activity_view)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.picture_menu, menu)
        if (!intent.getBooleanExtra(EXTRA_SHOW_DELETE_BUTTON, false)) {
            menu.findItem(R.id.picture_button_remove)?.isVisible = false
        }
        return true
    }

    override fun onBackPressed() {
        setResult(RESULT_OK)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.picture_button_remove -> {
            setResult(RESULT_CANCELED)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
