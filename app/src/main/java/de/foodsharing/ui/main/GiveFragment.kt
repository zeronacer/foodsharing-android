package de.foodsharing.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment

class GiveFragment : BaseFragment(), Injectable {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_give, container, false)

        return view
    }
}
