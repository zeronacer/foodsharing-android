package de.foodsharing.ui.baskets

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.R
import java.util.Date

class BasketDateFormatter(val context: Context) {

    fun format(date: Date): String {
        val time = DateFormatter.format(date, DateFormatter.Template.TIME)
        return when {
            DateFormatter.isToday(date) -> "${context.getString(R.string.date_today)} $time"
            DateFormatter.isYesterday(date) -> "${context.getString(R.string.date_yesterday)} $time"
            DateFormatter.isCurrentYear(date) ->
                "${DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH)} $time"
            else -> "${DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)} $time"
        }
    }
}
