package de.foodsharing.ui.editbasket

import android.util.Log
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.services.BasketService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.LOG_TAG
import java.io.File
import java.io.IOException
import javax.inject.Inject

class EditBasketViewModel @Inject constructor(
    private val baskets: BasketService
) : BaseViewModel() {

    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val basket = MutableLiveData<Basket>()
    val basketUpdated = MutableLiveData<Event<Unit>>()

    /**
     * @param description a new description or null, in which case the description will not be changed
     * @param coordinate a new coordinate or null, in which case the coordinate will not be changed
     * @param picture a new picture or null, in which case the picture will be deleted
     * @param pictureChanged if false, the basket's picture will not be changed and the picture parameter
     * will be ignored
     */
    fun update(description: String?, coordinate: Coordinate?, picture: File?, pictureChanged: Boolean) {
        basket.value?.let { b ->
            val desc = description ?: b.description
            val coord = coordinate ?: b.toCoordinate()

            isLoading.value = true

            var observable = baskets.update(b.id, desc, coord)
            Log.e(LOG_TAG, "observable 1: $observable")
            if (pictureChanged) {
                observable = observable.switchMap {
                    Log.e(LOG_TAG, "test upload")
                    changePictureObservable(b, picture)
                }
                Log.e(LOG_TAG, "observable 2: $observable")
            }

            request(observable, {
                isLoading.value = false
                basketUpdated.value = Event(Unit)
            }, {
                isLoading.value = false
                handleError(it)
            })
        }
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is IOException) {
            R.string.error_no_connection
        } else {
            error.printStackTrace()
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }

    private fun changePictureObservable(basket: Basket, picture: File?) =
        if (picture != null)
            baskets.setPicture(basket.id, picture)
        else
            baskets.removePicture(basket.id)
}
