package de.foodsharing.ui.editbasket

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.PickLocationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity.Companion.EXTRA_BASKET
import de.foodsharing.ui.picture.PictureFragment
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.DETAIL_MAP_ZOOM
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import kotlinx.android.synthetic.main.activity_edit_basket.basket_description_input
import kotlinx.android.synthetic.main.activity_edit_basket.basket_location_view
import kotlinx.android.synthetic.main.activity_edit_basket.basket_picture_view
import kotlinx.android.synthetic.main.activity_edit_basket.basket_update_button
import kotlinx.android.synthetic.main.activity_edit_basket.progress_bar
import kotlinx.android.synthetic.main.activity_edit_basket.toolbar
import javax.inject.Inject

/**
 * Activity that handles editing baskets. It requires a valid basket as an extra with
 * [EXTRA_BASKET].
 */
class EditBasketActivity : AuthRequiredBaseActivity(), Injectable {

    companion object {
        const val EXTRA_BASKET = "basket"

        private const val STATE_DESCRIPTION = "description"
        private const val STATE_COORDINATE = "coordinate"

        private const val REQUEST_PICK_LOCATION = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: EditBasketViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(EditBasketViewModel::class.java)
    }
    private var mapSnapshotDetachAction: (() -> Unit)? = null
    private lateinit var location: Coordinate
    private var pictureWasChanged = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.edit_basket_root
        setContentView(R.layout.activity_edit_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_edit_title)

        bindViewModel()

        val basket = intent.getSerializableExtra(EXTRA_BASKET) as Basket
        viewModel.basket.value = basket

        val pictureFragment = basket_picture_view as PictureFragment
        pictureFragment.setCanTakePhoto(true)
        if (!basket.picture.isNullOrEmpty()) {
            basket.picture?.let {
                val pictureUri = "$BASE_URL/images/basket/${basket.picture}"
                pictureFragment.pictureUri = pictureUri
            }
        }
        pictureFragment.pictureWasChanged.observe(this, EventObserver {
            pictureWasChanged = true
        })

        updateBasketMap(basket.toCoordinate())

        basket_update_button.setOnClickListener { updateBasket() }

        if (savedInstanceState == null) {
            basket_description_input.setText(basket?.description)
        } else if (savedInstanceState.containsKey(STATE_DESCRIPTION)) {
            basket_description_input.setText(savedInstanceState.getString(STATE_DESCRIPTION))
            updateBasketMap(savedInstanceState.getParcelable(STATE_COORDINATE)!!)
        }
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this, {
            progress_bar.visibility = if (it) VISIBLE else View.INVISIBLE
        })

        viewModel.showError.observe(this, EventObserver {
            progress_bar.visibility = GONE
            showMessage(getString(it))
            basket_update_button.isEnabled = true
        })

        viewModel.basketUpdated.observe(this, EventObserver {
            finish()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(STATE_DESCRIPTION, basket_description_input.text.toString())
        outState.putParcelable(STATE_COORDINATE, location)

        super.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PICK_LOCATION -> {
                if (resultCode == RESULT_OK && data != null) {
                    data.getParcelableExtra<Coordinate?>(PickLocationActivity.EXTRA_COORDINATE)
                        ?.let { updateBasketMap(it) }
                }
            }
        }
    }

    /**
     * Checks the input and updates the basket.
     */
    private fun updateBasket() {
        basket_update_button.isEnabled = false

        val description = basket_description_input.text.toString().trim()
        if (description.isEmpty()) {
            showMessage(getString(R.string.basket_error_description), Snackbar.LENGTH_LONG)
            basket_update_button.isEnabled = true
        } else {
            progress_bar.visibility = VISIBLE
            val pictureFile = (basket_picture_view as PictureFragment).file
            Log.e(LOG_TAG, "pictureChanged: $pictureWasChanged, file: ${pictureFile?.toString()}")
            viewModel.update(description, location, pictureFile, pictureWasChanged)
        }
    }

    private fun updateBasketMap(location: Coordinate) {
        this.location = location

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            basket_location_view,
            location,
            DETAIL_MAP_ZOOM,
            getBasketMarkerIconBitmap(applicationContext),
            preferences.allowHighResolutionMap
        )

        basket_location_view.setOnClickListener {
            val intent = Intent(this, PickLocationActivity::class.java)
            intent.putExtra(PickLocationActivity.EXTRA_COORDINATE, location)
            intent.putExtra(PickLocationActivity.EXTRA_MARKER_ID, R.string.marker_basket_id)
            intent.putExtra(PickLocationActivity.EXTRA_ZOOM, DETAIL_MAP_ZOOM)
            startActivityForResult(intent, REQUEST_PICK_LOCATION)
        }
    }
}
