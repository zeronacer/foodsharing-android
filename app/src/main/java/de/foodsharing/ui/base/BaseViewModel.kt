package de.foodsharing.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel : ViewModel() {
    protected val disposables = CompositeDisposable()

    fun <T> request(
        observable: Observable<T>,
        consumer: (T) -> Unit = {},
        onError: ((Throwable) -> Unit)
    ) {
        disposables += observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(consumer, onError)
    }

    fun request(
        observable: Completable,
        consumer: () -> Unit = {},
        onError: ((Throwable) -> Unit)
    ) {
        disposables += observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(consumer, onError)
    }

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }
}
