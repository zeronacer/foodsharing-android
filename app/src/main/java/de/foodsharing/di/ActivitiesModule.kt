package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.notifications.ConversationReplyBroadcastReceiver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.conversations.ShareTextToConversationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity
import de.foodsharing.ui.fsp.FoodSharePointActivity
import de.foodsharing.ui.initial.InitialActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.ui.newbasket.NewBasketActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.settings.SettingsActivity
import de.foodsharing.ui.users.UserListActivity

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun bindInitialActivity(): InitialActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindConversationActivity(): ConversationActivity

    @ContributesAndroidInjector
    abstract fun bindBasketActivity(): BasketActivity

    @ContributesAndroidInjector
    abstract fun bindNewBasketActivity(): NewBasketActivity

    @ContributesAndroidInjector
    abstract fun bindProfileActivity(): ProfileActivity

    @ContributesAndroidInjector
    abstract fun bindUserListActivity(): UserListActivity

    @ContributesAndroidInjector
    abstract fun bindEditBasketActivity(): EditBasketActivity

    @ContributesAndroidInjector
    abstract fun bindFoodSharePointActivity(): FoodSharePointActivity

    @ContributesAndroidInjector
    abstract fun bindShareTextToConversation(): ShareTextToConversationActivity

    @ContributesAndroidInjector
    abstract fun bindSettingsConversation(): SettingsActivity

    @ContributesAndroidInjector
    abstract fun bindConversationReplyBroadcastReceiver(): ConversationReplyBroadcastReceiver
}
